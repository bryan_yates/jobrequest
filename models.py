from app import db
import datetime



class Building(db.Model):
    __tablename__ = 'building'
    def to_dict(self):
        return {i.name: getattr(self, i.name) for i in self.__table__.columns}    

    #DB columns
    initials = db.Column(db.String(10), primary_key=True) # Unicode?
    fullname = db.Column(db.Unicode, nullable=False) # Unicode requires no length?
	assigned_tech = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)


class User(db.Model):
    __tablename__ = 'user'
    def to_dict(self):
        return {i.name: getattr(self, i.name) for i in self.__table__.columns}

    #DB columns
    id = db.Column(db.Integer, primary_key=True) # autokey?, return on creation?
    firstname = db.Column(db.Unicode, nullable=False)
    lastname = db.Column(db.Unicode, nullable=False)
    building = db.Column(db.String(10), db.ForeignKey('building.initials'), nullable=True)
    limited_to_my_building = db.Column(db.String(1)) # boolean??
    tech = db.Column(db.String(1)) # can change tech_assigned, boolean??
    email = db.Column(db.String, nullable=True) # default unstoppable update notifications
    cellphone = db.Column(db.Integer, nullable=True) # int vs. char?, update notif. option

class Jobrequest(db.Model):
    __tablename__ = 'jobrequest'
    def to_dict(self):
        return {i.name: getattr(self, i.name) for i in self.__table__.columns}

    #DB columns
    id = db.Column(db.Integer, primary_key=True) # autokey
    subject = db.Column(db.String(720))
    tech_assigned = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    room = db.Column(db.String(32))
    building = db.Column(db.String(32))
    phone = db.Column(db.String(32))
    update_user = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)
    update_datetime = db.Column(db.DateTime, nullable=True)
    create_user = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    create_datetime = db.Column(db.DateTime, default=datetime.datetime.now)

class JRText(db.Model):
	__tablename__ = 'jrtext'
	def to_dict(self):
		return {i.name: getattr(self, i.name) for i in self.__table__.columns}

    #DB columns
    # autokey?
    id = db.Column(db.Integer, primary_key=True) # autokey
	jrid = db.Column(db.Integer, db.ForeignKey('jobrequest.id'), nullable=False)
	# maybe a decimal or two here might be better
	hours =  db.Column(db.Integer, nullable=True)
	submit_user = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    submit_datetime = db.Column(db.DateTime, default=datetime.datetime.now)

class Followers(db.Model):
	__tablename__ = 'jrtext'
	def to_dict(self):
		return {i.name: getattr(self, i.name) for i in self.__table__.columns}
	userid = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
	role =  db.Column(db.Integer, nullable=False) # 1 = tech, 2 = non-tech, 3=?
	
